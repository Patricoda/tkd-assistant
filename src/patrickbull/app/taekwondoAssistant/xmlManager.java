package patrickbull.app.taekwondoAssistant;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.util.Xml;

//holds all xml parsing functionality.
public class xmlManager extends tagbActivity{

public ArrayList<InfoSection> readInformationXml(ArrayList<InfoSection> allInfo, Context context){
	ArrayList<String> terms = new ArrayList<String>();
    XmlPullParser Parser = context.getResources().getXml(R.xml.info);
    int EventType = -1;
	try{
		String title = "";
		 while(EventType != XmlResourceParser.END_DOCUMENT)
	        {
	        	if(EventType == XmlResourceParser.START_TAG){
	        		if (Parser.getName().equals("info")){
		        			title = Parser.getAttributeValue(null, "title");
	        		}
        			
	        		else if(Parser.getName().equals("term")){
	        				terms.add(Parser.getAttributeValue(null, "value"));
	        		}
	        		}
									EventType = Parser.next();
									if(EventType == XmlResourceParser.END_TAG){
										if(Parser.getName().equals("terms")){
				    	        			allInfo.add(new InfoSection(title,terms));
				    	        			terms.removeAll(terms);
										}
									}
	        	}
	}
	catch (XmlPullParserException e) {
		e.printStackTrace();
	} 
	catch (IOException e) {
		e.printStackTrace();
	}
	return allInfo;
}

public ArrayList<Questions> readQuestionXml(ArrayList<Questions> allQuestions, Context context){
    XmlPullParser Parser = context.getResources().getXml(R.xml.questions);
    int EventType = -1;
	try{
		 while(EventType != XmlResourceParser.END_DOCUMENT)
	        {
	        	if(EventType == XmlResourceParser.START_TAG){
	        		String strName = Parser.getName();
	        		if (strName.equals("question")){
		        			String grade = Parser.getAttributeValue(null, "grade");
		        			String text = Parser.getAttributeValue(null, "text");
		        			String correct = Parser.getAttributeValue(null, "correct");
		        			String answer1 = Parser.getAttributeValue(null, "answer1");
		        			String answer2 = Parser.getAttributeValue(null, "answer2");
		        			String answer3 = Parser.getAttributeValue(null, "answer3");
		        			String answer4 = Parser.getAttributeValue(null, "answer4");
		        		
		        			allQuestions.add(new Questions(grade, text, correct, answer1, answer2, answer3, answer4));
	        			}
	        		}
									EventType = Parser.next();
	        	}
	}
	catch (XmlPullParserException e) {
		e.printStackTrace();
	} 
	catch (IOException e) {
		e.printStackTrace();
	}
    Collections.shuffle(allQuestions);
	return allQuestions;
}

public void readUserXml(File file, ArrayList<User> users,Context context){
	try{
	if(file.exists()){
        XmlPullParser Parser = Xml.newPullParser();
		FileInputStream inputStream = new FileInputStream(context.getFilesDir() + "/" + "users.xml");
		InputStreamReader streamReader = new InputStreamReader(inputStream);
		
		Parser.setInput(streamReader);
        int EventType = -1;
        
		  while(EventType != XmlPullParser.END_DOCUMENT)
	        {
	        	if(EventType == XmlPullParser.START_TAG){
	        		String strName = Parser.getName();
	        		if (strName.equals("user")){
	        				int id = Integer.parseInt(Parser.getAttributeValue(null, "id"));
		        			String name = Parser.getAttributeValue(null, "name");
		        			int totalQuestionsCorrect = Integer.parseInt(Parser.getAttributeValue(null, "totalQuestionsCorrect"));
		        			int totalQuestionsAnswered = Integer.parseInt(Parser.getAttributeValue(null, "totalQuestionsAnswered"));
		        			int previousScore = Integer.parseInt(Parser.getAttributeValue(null, "previousScore"));
		        			int previousTotalQuestions = Integer.parseInt(Parser.getAttributeValue(null, "previousTotalQuestions"));
		        		
		        			users.add(new User(id, name, totalQuestionsCorrect, totalQuestionsAnswered, previousScore, previousTotalQuestions));
	        			}
	        		}
	        	
				EventType = Parser.next();	
	        }
			streamReader.close();
	}
	}

	catch (XmlPullParserException e) {
		e.printStackTrace();
	} 
	catch (IOException e) {
		e.printStackTrace();
	}
	catch(Exception e){
		e.printStackTrace();
	}
}

public void writeXml(ArrayList<User> user, Context context){
	ArrayList<User> userList = user;
    XmlSerializer xmlSerializer = Xml.newSerializer();
    StringWriter writer = new StringWriter(); 
    try {
    	xmlSerializer.setOutput(writer);
        xmlSerializer.startDocument("UTF-8", true);
        xmlSerializer.startTag("", "users");
        for (int i = 0; i<userList.size(); i++){
            xmlSerializer.startTag("", "user");
            xmlSerializer.attribute("", "id", String.valueOf(userList.get(i).getUserId()));
            xmlSerializer.attribute("", "name", userList.get(i).getName());
            xmlSerializer.attribute("", "totalQuestionsCorrect", String.valueOf(userList.get(i).getTotalQuestionsCorrect()));
            xmlSerializer.attribute("", "totalQuestionsAnswered", String.valueOf(userList.get(i).getTotalQuestionsAnswered()));
            xmlSerializer.attribute("", "previousScore", String.valueOf(userList.get(i).getPreviousScore()));
            xmlSerializer.attribute("", "previousTotalQuestions", String.valueOf(userList.get(i).getPreviousTotalQuestions()));
            xmlSerializer.endTag("", "user");
        }
        xmlSerializer.endTag("", "users");
        xmlSerializer.endDocument();
        save(writer.toString(), context);
    } catch (Exception e) {
        throw new RuntimeException(e);
    } 
}

public void save(String xml, Context context){
	 FileOutputStream fos;
	try {
		fos = context.openFileOutput("users.xml", MODE_PRIVATE);
     OutputStreamWriter osw = new OutputStreamWriter(fos);
     osw.write(xml);
     osw.flush();
     osw.close();
	}
	catch(IOException e){
		
	}
}
}
