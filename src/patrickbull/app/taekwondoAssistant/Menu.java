package patrickbull.app.taekwondoAssistant;




import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Menu extends tagbActivity {
	@Override
	public void onBackPressed() {
		finish();
	}

	//checks the user's stats to see if the results and reset data buttons should be enabled or disabled.
	
	public void checkHistory(){
        final Button resultsButton = (Button) findViewById(R.id.resultsBtn);
        final Button resetDataButton = (Button) findViewById(R.id.resetDataBtn);
		if(currentUser.getTotalQuestionsAnswered() == 0){
        	resultsButton.setEnabled(false);
        	resetDataButton.setEnabled(false);
        }
        else{
        	resultsButton.setEnabled(true);
        	resetDataButton.setEnabled(true);
        }
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        final int resetDataDialog = 0;
        final int deleteUserDialog = 1;
        final Button quizButton = (Button) findViewById(R.id.quizBtn);
        final Button resultsButton = (Button) findViewById(R.id.resultsBtn);
        final Button reviseButton = (Button) findViewById(R.id.reviseBtn);
        final Button resetDataButton = (Button) findViewById(R.id.resetDataBtn);
        final Button deleteUserButton = (Button) findViewById(R.id.deleteUserBtn);
        
        //button listeners.
        
        //ensures score variables are set to 0 for new quiz.
        
        quizButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	startActivity(new Intent("Settings"));
            	currentScore = 0;
            	currentQuestion = 0;
            }
        });
             
        resultsButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v){
                startActivity(new Intent("History"));
            }
        });
        
        reviseButton.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		startActivity(new Intent("Information"));
        	}
        });
        
        resetDataButton.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		showDialog(resetDataDialog);
        	}
        });
        
        deleteUserButton.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				showDialog(deleteUserDialog);
			}	
        });
        
       
    }

    @Override
    //dialog instantiation, different case for different dialogs.
    protected Dialog onCreateDialog(int id){
    	switch(id){
    		case(0):
    			AlertDialog.Builder resetBuilder = new AlertDialog.Builder(this);
    			resetBuilder.setMessage("Are you sure you want to reset user history?");
    			resetBuilder.setCancelable(false);
    			resetBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener(){
    		
    				public void onClick(DialogInterface dialog, int which){
							currentUser.setTotalQuestionsCorrect(0);
							currentUser.setTotalQuestionsAnswered(0);
							currentUser.setPreviousScore(0);
							currentUser.setPreviousTotalQuestions(0);
							checkHistory();
							xmlManager xmlmanager = new xmlManager();
							Context context = getBaseContext();
							xmlmanager.writeXml(users, context);
    				}
    			});
    	
    			resetBuilder.setNegativeButton("No", new DialogInterface.OnClickListener(){
    				public void onClick(DialogInterface dialog, int which) {
    					dialog.cancel();
    				}	
    			});
    	
    			AlertDialog confirmReset = resetBuilder.create();
    			return confirmReset;
    		
    			case(1):
    				AlertDialog.Builder deleteBuilder = new AlertDialog.Builder(this);
    			deleteBuilder.setMessage("Are you sure you want to delete this account?");
    			deleteBuilder.setCancelable(false);
    			deleteBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener(){
    				
    				public void onClick(DialogInterface dialog, int which){
    					for(int i = 0; i<users.size(); i++){
    						if(currentUser.getUserId() == users.get(i).getUserId()){
    							users.remove(i);
    							xmlManager xmlmanager = new xmlManager();
    							Context context = getBaseContext();
    							xmlmanager.writeXml(users, context);
    							finish();
    						}
    					}
    				}
    			});
    			
    			deleteBuilder.setNegativeButton("No", new DialogInterface.OnClickListener(){
    				public void onClick(DialogInterface dialog, int which){
    					dialog.cancel();
    				}
    			});
    			
    			AlertDialog confirmDelete = deleteBuilder.create();
    			return confirmDelete;
    		}
		return null;
    }
    
    //updates menu buttons, incase user resets data.
    protected void onResume(){
    	super.onResume();
    	checkHistory();
    }
}
