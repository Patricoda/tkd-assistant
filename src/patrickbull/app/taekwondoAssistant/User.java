package patrickbull.app.taekwondoAssistant;

public class User {
	private int userId = 0;
	private String name = "";
	private int totalQuestionsCorrect = 0;
	private int totalQuestionsAnswered = 0;
	private int previousScore = 0;
	private int previousTotalQuestions = 0;

	public User(int id,String n,int tqc, int tqa, int ps, int ptq){
		userId = id;
		name = n;
		totalQuestionsCorrect = tqc;
		totalQuestionsAnswered = tqa;
		previousScore = ps;
		previousTotalQuestions = ptq;
	}
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getTotalQuestionsCorrect() {
		return totalQuestionsCorrect;
	}
	public void setTotalQuestionsCorrect(int totalQuestions) {
		totalQuestionsCorrect = totalQuestions;
	}
	public int getTotalQuestionsAnswered() {
		return totalQuestionsAnswered;
	}
	public void setTotalQuestionsAnswered(int totalQuestions) {
		totalQuestionsAnswered = totalQuestions;
	}
	public int getPreviousScore() {
		return previousScore;
	}
	public void setPreviousScore(int pScore) {
		previousScore = pScore;
	}
	public int getPreviousTotalQuestions() {
		return previousTotalQuestions;
	}
	public void setPreviousTotalQuestions(int previousTotal) {
		previousTotalQuestions = previousTotal;
	}
}
