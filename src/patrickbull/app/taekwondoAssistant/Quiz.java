package patrickbull.app.taekwondoAssistant;


import java.util.ArrayList;
import java.util.Timer;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

	public class Quiz extends tagbActivity{
        ArrayList<Questions> allQuestions = new ArrayList<Questions>();
		@Override
		public void onBackPressed() {
			finish();
		}

		//functionality to set a new question instance.
		
        private void setQuestion(Questions[] array,TextView questionTitle, TextView question, Button btn1, Button btn2, Button btn3, Button btn4)
        {
        	Questions[] questions = array;
        	TextView currentTitle = questionTitle;
        	TextView currentQ = question;
        	Button buttonA = btn1;
        	Button buttonB = btn2;
        	Button buttonC = btn3;
        	Button buttonD = btn4;
        	
			if (currentQuestion == questions.length){
				finish();
				startActivity(new Intent("Results"));
			}
			
			else{
			currentTitle.setText("Question " + (currentQuestion + 1) + " of " + questions.length);
        	currentQ.setText(questions[currentQuestion].getText());
        	buttonA.setText(questions[currentQuestion].getAnswer1());
        	buttonB.setText(questions[currentQuestion].getAnswer2());
        	buttonC.setText(questions[currentQuestion].getAnswer3());
        	buttonD.setText(questions[currentQuestion].getAnswer4());
			}
        }
        
        private void clickCheck( Questions[] curQuestions,Button btn1, Button btn2, Button btn3, Button btn4){
        	Button[] myButtons = new Button[]{btn1, btn2, btn3, btn4};
        	Questions[] currentQuestions = curQuestions;    
        	myButtons[0].setText(currentQuestions[currentQuestion].getAnswer1());
        	myButtons[1].setText(currentQuestions[currentQuestion].getAnswer2());
        	myButtons[2].setText(currentQuestions[currentQuestion].getAnswer3());
        	myButtons[3].setText(currentQuestions[currentQuestion].getAnswer4());
        	for(int i = 0; i < myButtons.length; i++ )
        	{
        		//if correct, button background changes to green, otherwise, red.
        		
        		if (myButtons[i].getText() == currentQuestions[currentQuestion].getCorrect()){
        			myButtons[i].getBackground().setColorFilter(new LightingColorFilter(0xFF00FF00, 0xFF00DD00));
        			if(myButtons[i].isPressed()){
        				currentScore++;
        				currentUser.setTotalQuestionsCorrect(currentUser.getTotalQuestionsCorrect() + 1);
        			}
        		}
        		else{
        			myButtons[i].getBackground().setColorFilter(new LightingColorFilter(0xFFFF0000, 0xFFAA0000));
        		}
        		
        		myButtons[i].setClickable(false);
        	}
        	currentQuestion++;
        	currentUser.setTotalQuestionsAnswered(currentUser.getTotalQuestionsAnswered() + 1);
        	xmlManager xmlmanager = new xmlManager();
	        Context context = getBaseContext();
	        
	        //writes updated figures instantly to ensure figures are accurate even if the quiz is closed before finishing.
	        
			xmlmanager.writeXml(users, context);
        }
        
        //return button colours to default and ensures they are clickable again.
        
        private void returnToDefault(Button btn1, Button btn2, Button btn3, Button btn4){
        	Button[] myButtons = new Button[]{btn1, btn2, btn3, btn4};
        	
        	for (int i = 0; i < myButtons.length; i++){
        		myButtons[i].getBackground().setColorFilter(null);
        		myButtons[i].setClickable(true);
        	}
        	}
        
        /*creates UI instance, having this functionality in a seperate function other than onCreate
          ensures orientation changes do not change the question or call null pointer errors.
        */
        
        public void createUI(){
        	final TextView questionTitle = (TextView) findViewById(R.id.questionTitle);
	        final TextView question = (TextView) findViewById(R.id.questionText);
	        final Button button1 = (Button) findViewById(R.id.answerBtn1); 
	        final Button button2 = (Button) findViewById(R.id.answerBtn2); 
	        final Button button3 = (Button) findViewById(R.id.answerBtn3);
	        final Button button4 = (Button) findViewById(R.id.answerBtn4);
			int x = 0;
    		final Timer time = new Timer();
	        
    		//ensures correct question length is chosen.
    		
	        final Questions[] questionArray = checkQuestionsLength(allQuestions);
	        
	        //selects the correct questions from the allQuestions list and inserts them into questionArray.
	        		if(includePreviousQuestions == true){
	        			for(int b = 0; b < allQuestions.size() ; b++){
	        				if (allQuestions.get(b).getGrade() >= questionGrade){
	        					questionArray[x] = allQuestions.get(b);
	        					x++;
	        				}
	        				
	        				if(x == questionArray.length){break;}
	        			}}
	        		
	        		else{
	        			for(int b = 0; b < allQuestions.size() ; b++){
	        				if(allQuestions.get(b).getGrade() == questionGrade){
	        					questionArray[x] = allQuestions.get(b);
	        					x++;
	        				}
	        				
	        				if(x == questionArray.length){break;}
	        			}}

	        setQuestion(questionArray, questionTitle, question, button1, button2, button3, button4);
	        
	        //functionality to be used with timer.
    		final Runnable runCode = new Runnable(){
    			public void run() {  
                	returnToDefault(button1, button2, button3, button4);
                	setQuestion(questionArray, questionTitle, question, button1, button2, button3, button4);
                }};
    		
             //when button is clicked, returnToDefault and setQuestion are called after 2000ms.
    		 View.OnClickListener listen = new OnClickListener(){
 	        	public void onClick(View v){
 	        		clickCheck(questionArray,button1, button2, button3, button4);
 	        		final timerTask task = new timerTask(runCode); //need new instance of timertask each time
 	        		time.schedule(task, 2000);
 	        	}};
 	        	
 	        button1.setOnClickListener(listen);
	        button2.setOnClickListener(listen);
	        button3.setOnClickListener(listen);
	        button4.setOnClickListener(listen);
        }
	    
	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.quiz);
    		Context context = getBaseContext();
	        xmlManager xmlmanager = new xmlManager();
	        allQuestions = xmlmanager.readQuestionXml(allQuestions, context);
	        createUI();
	    }

	    
	    private Questions[] checkQuestionsLength(ArrayList<Questions> questions){
	    	ArrayList<Questions> allQuestions = questions;
	    		    	
	    	int arraySize = 0;
	    	if(addAllQuestions == true){
	    		if(includePreviousQuestions == true){
	    		for(int i = 0; i < allQuestions.size(); i++)
	    		{
	    				if(allQuestions.get(i).getGrade() >= questionGrade){
	    					arraySize++;
	    				}
	    		}
	    		totalQuestions = arraySize;
	    		Questions[] curQuestions = new Questions[arraySize];
	    		return curQuestions;
	    		}
	    		
	    		
	    	else{
	    		for(int i = 0; i < allQuestions.size(); i++){
	    			if(allQuestions.get(i).getGrade() == questionGrade){
	    				arraySize++;
	    			}
	    		}
	    		totalQuestions = arraySize;
	    		Questions[] curQuestions = new Questions[arraySize];
	    		return curQuestions;
	    		}
	    	}
	    	
	    	Questions[] curQuestions = new Questions[totalQuestions];
	    	return curQuestions;
	    }
	    
	    //ensures correct orientation and creates the UI, ensures activity isn't reinitialised but uses this function instead.
	    /*public void onConfigurationChanged(Configuration newConfig){
	    	super.onConfigurationChanged(newConfig);
	    	setContentView(R.layout.quiz);
	    	createUI();
	    }*/
	}