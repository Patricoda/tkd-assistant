package patrickbull.app.taekwondoAssistant;

import java.util.ArrayList;
import android.app.Activity;

//child class of activity and parent class to all other activities. Holds values to be used across all other activities.
public class tagbActivity extends Activity {
	protected static ArrayList<User> users;
	protected static User currentUser = new User(0,null, 0, 0, 0, 0);
	protected static int currentQuestion = 0;
	protected static int currentScore = 0;
	protected static int totalQuestions = 0;
	protected static int questionGrade = 0;
	protected static boolean addAllQuestions = false;
	protected static boolean includePreviousQuestions = false;
}