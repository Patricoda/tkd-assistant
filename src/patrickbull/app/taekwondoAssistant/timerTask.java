package patrickbull.app.taekwondoAssistant;

import java.util.TimerTask;

import android.os.Handler;

//custom timertask to implement handler needed when using timers on Android device.

public class timerTask extends TimerTask {
	
Handler timeHandle = new Handler();
Runnable runCode;


	public timerTask(Runnable run) {
		runCode = run;
	}

	@Override
	public void run() {
		timeHandle.post(runCode);
	}

}
