package patrickbull.app.taekwondoAssistant;


import android.os.Bundle;
import android.widget.RatingBar;
import android.widget.TextView;
	

	public class History extends tagbActivity {
		
		@Override
		public void onBackPressed() {
			finish();
		}

	    /** Called when the activity is first created. */
	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.history);
	        final TextView previousScored = (TextView) findViewById(R.id.prevScoreDisplay);
	        final TextView previousPercent = (TextView) findViewById(R.id.prevScorePercentage);
	        final TextView previousRated = (TextView) findViewById(R.id.prevScoreRating);
	    	final TextView questionsCorrect = (TextView) findViewById(R.id.questionsCorrect);
	    	final TextView questionsAnswered = (TextView) findViewById(R.id.questionsAnswered);
	    	final TextView percent = (TextView) findViewById(R.id.percentageCorrect);
	    	final TextView rating = (TextView) findViewById(R.id.totalRating);
	        final RatingBar prevScoreRatingBar = (RatingBar) findViewById(R.id.prevScoreRatingBar);
	        final RatingBar totalRatingBar = (RatingBar) findViewById(R.id.totalRatingBar);
	        float percentageNumber = ((float) currentUser.getTotalQuestionsCorrect() / (float) currentUser.getTotalQuestionsAnswered()) * 100;
	        int percentage = (int)percentageNumber;
	        float previousPercentage = ((float)currentUser.getPreviousScore() / (float)currentUser.getPreviousTotalQuestions()) * 100;
	        int prevPercentage = (int)previousPercentage;
	        previousScored.append(" " + currentUser.getPreviousScore() + "/" + currentUser.getPreviousTotalQuestions());
	        previousPercent.append(" " + prevPercentage + "%");
	        questionsCorrect.append(" " + currentUser.getTotalQuestionsCorrect());
	        questionsAnswered.append(" " + currentUser.getTotalQuestionsAnswered());
	        percent.append(" " + percentage + "%");
	        
	        //calls static function to set star rating.
	        Results.setStars(prevScoreRatingBar, prevPercentage, previousRated);
	        Results.setStars(totalRatingBar, percentage, rating);
	    }
	    

	}

