package patrickbull.app.taekwondoAssistant;

import java.util.ArrayList;

public class InfoSection {
	String title = "";
	ArrayList<String> terms = new ArrayList<String>();
	
	public InfoSection(String title, ArrayList<String> terms){
		this.title = title;
		this.terms.addAll(terms);
	}

	public String getTitle(){
		return this.title;
	}
	
	public ArrayList<String> getTerms(){
		return this.terms;
	}
}
