package patrickbull.app.taekwondoAssistant;

import java.io.File;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

//activity thats launched when app is initiated.

public class Main extends tagbActivity {
	
	//ensures app closes fully when back button is pressed.
	
	public void onBackPressed() {
		finish();
	}
	
	/*on creation of the activity, we instantiate users as a new arrayList and load
	  in the user data if present from the users.xml file in internal storage.
	 */
	
	public void onCreate(Bundle savedInstanceState){
		users = new ArrayList<User>();
		File file = getFileStreamPath("users.xml");
		Context context = getBaseContext();
        xmlManager xmlmanager = new xmlManager();
        xmlmanager.readUserXml(file, users, context);
        
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		Button addUser = (Button) findViewById(R.id.addUserBtn);
		final int addUserDialog = 0;

		addUser.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0) {
				showDialog(addUserDialog);
			}
		});
	}
	
	//sets UI list of users and set a listener to ensure the user selected is made the current user.
	
	public void setList(){
		final ListView listView = (ListView)findViewById(R.id.list);
		listView.setAdapter(new UserListView(this, R.layout.listviewitem, users));
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

				public void onItemClick(AdapterView<?> arg0, View arg1,int thisItem, long arg3) {
					for(int i = 0; i < users.size(); i++)
					{
					if((listView.getItemAtPosition(thisItem)) == users.get(i))
					{
						currentUser = users.get(i);
						startActivity(new Intent("Menu"));
					}
					}
				}
				
			});
	}
	
	//updates the list when activity is resumed from dialog or main menu in case of delete or new user.
	
	public void onResume(){
		super.onResume();
		setList();
	}
	
	//new user dialog initialisation, button override to change the automatic close behaviour.
	
   protected Dialog onCreateDialog(int id){
    	LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	final View layout = inflater.inflate(R.layout.adduserdialog, (ViewGroup) findViewById(R.id.dialogLayout));
    	final Builder builder = new Builder(this);
    	builder.setView(layout);
    	builder.setTitle("Add New User");
    	final EditText name = (EditText) layout.findViewById(R.id.enterName);
    	final TextView nameProb = (TextView) layout.findViewById(R.id.nameProb);
    	builder.setCancelable(false);

    	builder.setPositiveButton("Add User", new DialogInterface.OnClickListener(){
    		
			public void onClick(DialogInterface custom, int which){

			}
    	});
    	
    	builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}	
    	});
    	
    	final AlertDialog addUser = builder.create();

    	addUser.setOnShowListener(new DialogInterface.OnShowListener() { 
             public void onShow(DialogInterface dialog) { 

                 Button positiveButton = addUser.getButton(AlertDialog.BUTTON_POSITIVE); 
                 positiveButton.setOnClickListener(new View.OnClickListener() { 
                	 
                     public void onClick(View view) { 
         				if(!name.getText().toString().equals("")){
         					int i = 0;
         					if(users.isEmpty())
         					{i = 0;}
         					else
         					{i = users.get(users.size()-1).getUserId()+1;}
         					
         					users.add(new User(i, name.getText().toString(), 0, 0, 0, 0));
         						xmlManager xmlmanager = new xmlManager();
         				        Context context = getBaseContext();
         						xmlmanager.writeXml(users, context);
         						name.setText("");
         						addUser.dismiss(); 
         						setList();
         					}
         					else{
         						nameProb.setText(R.string.nameProb);
         					}
                     } 
                 }); 
             } 
         }); 

    	return addUser;
    }
}
