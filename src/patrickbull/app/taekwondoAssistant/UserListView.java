package patrickbull.app.taekwondoAssistant;

import java.util.List;




import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

//overridden functionality to allow user list to encorporate more than just string values such as rating bar,etc.
public class UserListView extends ArrayAdapter<User>{
	int resource;
	Context context;
	String response;
	LayoutInflater layoutInflater;
	
	public UserListView(Context context, int resource, List<User> users){
		super(context,resource,users);
		String inflater = Context.LAYOUT_INFLATER_SERVICE;
		layoutInflater = (LayoutInflater)getContext().getSystemService(inflater);
		this.resource=resource;
	}

	public View getView(int position, View convertView, ViewGroup parent){ 
	    User user = getItem(position);   
	    
	    if(convertView == null) 
	    { 
	        convertView = layoutInflater.inflate(resource, parent, false); 
	    } 
	 
	    TextView name = (TextView) convertView.findViewById(R.id.name); 
	    RatingBar userRating = (RatingBar) convertView.findViewById(R.id.userRatingBar); 
	    name.setText(String.valueOf(user.getName())); 
	 
	    float percentageNumber = ((float) user.getTotalQuestionsCorrect() / (float) user.getTotalQuestionsAnswered()) * 100; 
	    int percentage = (int)percentageNumber; 
	    setStars(userRating, percentage); 
	    return convertView; 
	} 

	

	private void setStars(RatingBar rb, int percent){
    	RatingBar starRating = rb;
    	int percentage = percent;
    	
    	if(percentage <= 5){starRating.setRating(0);}
    	else if(percentage < 25){starRating.setRating(1);}
        else if(percentage >= 25 && percentage < 50){starRating.setRating(2);}
        else if(percentage >= 50 && percentage < 75){starRating.setRating(3);}
        else if(percentage >= 75 && percentage < 100){starRating.setRating(4);}
        else{starRating.setRating(5);}
    }
	
}
