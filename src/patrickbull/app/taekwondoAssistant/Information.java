package patrickbull.app.taekwondoAssistant;


import java.util.ArrayList;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

public class Information extends tagbActivity {
	@Override
	public void onBackPressed() {
		finish();
	}
	
	//sets adapter items, depending on the grade selected.
	public void setAdapter(int i){
        ListView listView = (ListView) findViewById(R.id.list);
		Context context = getBaseContext();
	    ArrayList<InfoSection> allInfo = new ArrayList<InfoSection>();
	    xmlManager xmlmanager = new xmlManager();
	    xmlmanager.readInformationXml(allInfo, context);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.infolistitem, allInfo.get(i).getTerms()){  
            public boolean areAllItemsEnabled() 
            {  
                    return false;  
            }  
            public boolean isEnabled(int position)  
            {  
                    return false;  
            }  
        };
        
        listView.setAdapter(adapter);
	}
    /** Called when the activity is first created. */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.information);
        final Spinner gradeSpinner = (Spinner) findViewById(R.id.title);
        final String[] grades = getResources().getStringArray(R.array.questionGrade);

        int i = 0; //index for terms list.
      
        ArrayAdapter<String> adapterS = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, grades);
        gradeSpinner.setAdapter(adapterS);
        setAdapter(i);
        
        //ensures content changes when a different grade is selected from list.
        gradeSpinner.setOnItemSelectedListener(new OnItemSelectedListener(){

			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				int i = gradeLevel(gradeSpinner, grades);
				setAdapter(i);			
			}

			public void onNothingSelected(AdapterView<?> arg0) {

			}
        });

        
    }
    
    //returns correct grade value from spinner.
    private int gradeLevel(Spinner gspin, String Array[]){
    	Spinner gSpin = gspin;
    	String grades[] = Array;
    	String grade;
    	int gradeLevel = 0;
    	
    	grade = (String) gSpin.getSelectedItem();
    	
    	for(int i =0; i < grades.length;i++){
    		if(grade == grades[i]){
    			gradeLevel = i;
    			break;
    		}
    	}
    	return gradeLevel;
    }
}
