package patrickbull.app.taekwondoAssistant;


import android.content.Context;
import android.os.Bundle;
import android.widget.RatingBar;
import android.widget.TextView;
	

	public class Results extends tagbActivity {
		
		@Override
		public void onBackPressed() {
			finish();
		}

	    /** Called when the activity is first created. */
	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.results);
	    	final TextView score = (TextView) findViewById(R.id.scoreDisplay);
	    	final TextView percent = (TextView) findViewById(R.id.scorePercentage);
	    	final TextView rating = (TextView) findViewById(R.id.scoreRating);
	    	final RatingBar starBar = (RatingBar) findViewById(R.id.ratingBar);
	        float percentageNumber = ((float) currentScore / (float) totalQuestions) * 100;
	        int percentage = (int)percentageNumber;
	        score.append(" " + currentScore + "/" + totalQuestions);
	        percent.append(" " + percentage + "%");
	        currentUser.setPreviousScore(currentScore);
	        currentUser.setPreviousTotalQuestions(totalQuestions);
	        setStars(starBar, percentage, rating);
	        xmlManager xmlmanager = new xmlManager();
	        Context context = getBaseContext();
			xmlmanager.writeXml(users, context);
	    }
	    
	    //calculates rating and sets stars depending on this rating, static to allow easy access from other activities.
	    protected static void setStars(RatingBar rb, int percent, TextView r){
	    	RatingBar starRating = rb;
	    	int percentage = percent;
	    	TextView rating = r;
	    	
	    	if(percentage <= 5){rating.append(" Awful"); starRating.setRating(0);}
	    	else if(percentage < 25){rating.append(" Poor"); starRating.setRating(1);}
	        else if(percentage >= 25 && percentage < 50){rating.append(" Low"); starRating.setRating(2);}
	        else if(percentage >= 50 && percentage < 75){rating.append(" Good"); starRating.setRating(3);}
	        else if(percentage >= 75 && percentage < 100){rating.append(" Excellent"); starRating.setRating(4);}
	        else{rating.append(" Perfect"); starRating.setRating(5);}
	    }
	}

