package patrickbull.app.taekwondoAssistant;

import java.util.Arrays;
import java.util.Collections;

public class Questions {
	private int grade;
	private String text;
	private String correct;
	private String answers[] = new String[4];

	
	
	public Questions(String qgrade, String qtext, String qcorrect, String qanswer1, String qanswer2, String qanswer3, String qanswer4){
		grade = Integer.parseInt(qgrade);
		text = qtext;
		correct = qcorrect;
		answers[0] = qanswer1;
		answers[1] = qanswer2;
		answers[2] = qanswer3;
		answers[3] = qanswer4;
		
		//shuffles answers in constructor to ensure answers are never in the same position.
		Collections.shuffle(Arrays.asList(answers));
	}
	
	public int getGrade(){
		return grade;
	}
	
	public String getText(){
		return text;
	}
	
	public String getCorrect(){
		return correct;
	}
	
	public String getAnswer1(){
		return answers[0];
	}
	
	public String getAnswer2(){
		return answers[1];
	}
	
	public String getAnswer3(){
		return answers[2];
	}
	
	public String getAnswer4(){
		return answers[3];
	}
	
	
}