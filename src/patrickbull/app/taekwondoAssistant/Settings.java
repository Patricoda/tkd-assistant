package patrickbull.app.taekwondoAssistant;





import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.Toast;
	

	public class Settings extends tagbActivity {
		@Override
		public void onBackPressed() {
			finish();
		}

	    /** Called when the activity is first created. */
	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.settings);
	        final Button startBtn = (Button) findViewById(R.id.startBtn);
	        final String[] numberOfQuestions = getResources().getStringArray(R.array.questionNumber);
	        final String[] grades = getResources().getStringArray(R.array.questionGrade);
	        final Spinner qSpin = (Spinner) findViewById(R.id.qSpinner);
	        final Spinner gSpin = (Spinner) findViewById(R.id.gSpinner);
	        final CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox);
	        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, numberOfQuestions);
	        ArrayAdapter<String> adapterS = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, grades);
	        qSpin.setAdapter(adapter);
	        gSpin.setAdapter(adapterS);
	        
	        Context context = getApplicationContext();
			final Toast toast =Toast.makeText(context, "There are only 10 questions for this grade", Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			
			includePreviousQuestions = false;
			addAllQuestions = false;
			
			checkBox.setOnClickListener(new OnClickListener(){
				public void onClick(View v){
					if(checkBox.isChecked()){
						includePreviousQuestions = true;
					}
					else{
						includePreviousQuestions = false;
					}
			}});
			
			//ensures correct value is set in totalQuestions.
	        startBtn.setOnClickListener(new View.OnClickListener(){
	        	public void onClick(View v){
	        		String numberQuestions = (String) qSpin.getSelectedItem();
	        		if(!numberQuestions.equals("All Questions")){
	        			totalQuestions = Integer.parseInt(numberQuestions);
	        		}
	        		else{
	        			addAllQuestions = true;
	        		}
	        		questionGrade = gradeLevel(gSpin, grades);
	        		if(questionGrade == 2 && totalQuestions == 15 && includePreviousQuestions == false){
	        			totalQuestions = 10;
	        			toast.show();
	        		}
	        		startActivity(new Intent("Quiz"));
	        		finish();
	        	}
	        });
	    }
        
	    //returns the gradeLevel variable based on the item selected in the spinner element.
        private int gradeLevel(Spinner gspin, String Array[]){
        	Spinner gSpin = gspin;
        	String grades[] = Array;
        	String grade;
        	int gradeLevel = 0;
        	
        	grade = (String) gSpin.getSelectedItem();
        	
        	for(int i = 0; i<grades.length;i++){
        		if(grade == grades[i]){
        			gradeLevel = 10 - i;
        		}
        	}
        	
        	return gradeLevel;
        }
	}